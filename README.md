Ideas for CETI Journal version

- Main: Experiments: add synthesis results,  is it possible to improve the Bug Repair results ? 
- Experiments:  Can we use KLEE to solve some other Sketch's problems ?  Use benchmark from Alur's program synthesis paper, could also use Sketch's gallery programs

- Expand text (using the Alur's program synthesis paper)
- Update related work

